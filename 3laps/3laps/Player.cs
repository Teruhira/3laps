﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace _3laps
{
    class Player : GameObject 
    {

        private float walkSpeed = 5, jumpWalkSpeed = 10f, jumpForce = -20;
        private int extraJump = 0, extraJumpCount;
        private float endPoint = 64 * 28;
        private bool isGrounded, isJumped, isJumping, jumpFlg, isDrop;
        private Rigidbody rb, footRb;
        private PlayerFoot playerFoot;
        private float jumpTime = 10, jumpTimeCount;
        private List<string> groundFilter = new List<string>() { "Map_Ground"};

        public Player(Transform transform) : base("Player", transform)
        {
            AddComponent(new SpriteRenderer("Player", transform, 0, 64));//画像の読み込み　表示は自動
            AddComponent(new RectangleCollider(this, new Square(transform, 64)));//当たり判定をつけた
            AddComponent(new Rigidbody(transform, (RectangleCollider)GetComponent("RectangleCollider")));//当たり判定を用いての衝突判定と速度分の移動をつけた
            rb = (Rigidbody)GetComponent("Rigidbody");
            playerFoot = (PlayerFoot)GameManager.instance.nowScene.gameObjectManager.Instantiate(new PlayerFoot(new Transform(new Vector2(transform.position.X, transform.position.Y + 32)), this));
            footRb = (Rigidbody)playerFoot.GetComponent("Rigidbody");
        }

        public override void Hit(RectangleCollider rectangleCollider)
        {
            
        }

        public override void Start()
        {
            
        }

        public override void Update()
        {   
            if(transform.position.Y > 64 * 19)
            {
                GameManager.instance.ChangeScene(new TestScene());
            }
            if (LevelManager.instance.levelState != LevelManager.LevelState.Play)
            {
                rb.velocity = Vector2.Zero;
                return;
            }
            Walk();
            Jump();
            Drop();
            if(endPoint <= transform.position.X)
            {
                transform.position.X = 64;
                LevelManager.instance.lapCount++;
            }
            
        }

        public void FootHit(RectangleCollider rectangleCollider)
        {

        }

        public void Walk()
        {
            if (isDrop)
            {
                rb.velocity.X = 0;
                return;
            }
            if (isJumped)
            {
                rb.velocity.X = jumpWalkSpeed;
            }
            else
            {
                rb.velocity.X = walkSpeed;//右へ移動
            }
        }

        public void Jump()
        {            
            playerFoot.SetPosition();//接地判定位置を更新
            isGrounded = GameManager.instance.nowScene.nowMap.MapCollision(footRb, true, groundFilter);//接地判定
            if (isGrounded)
            {
                extraJumpCount = extraJump;
                if (isDrop)
                {
                    isDrop = false;
                    rb.gravityScale = 1;
                }
                if (jumpFlg)
                {
                    isJumped = false;
                    jumpFlg = false;
                }
            }
            else
            {
                jumpFlg = true;
            }

            StartJump();
            HoldJump();           
        }

        public void Drop()
        {
            if(Input.GetKeyDown(Keys.Space) && !isGrounded && !isDrop)
            {
                isDrop = true;
                rb.gravityScale =  2;
            }
        }

        public void StartJump()
        {
            if (Input.GetKeyDown(Keys.Space) && isGrounded)
            {
                rb.velocity.Y = jumpForce;//ジャンプ
                jumpTimeCount = jumpTime;
                isJumped = true;
                isJumping = true;
            }
            else if (Input.GetKeyDown(Keys.Space) && extraJumpCount > 0)
            {
                rb.velocity.Y = jumpForce;
                jumpTimeCount = jumpTime;
                isJumping = true;
                extraJumpCount--;
            }
        }

        public void HoldJump()
        {
            if (isJumping)
            {
                if (Input.GetKey(Keys.Space))
                {
                    rb.velocity.Y = jumpForce;
                    jumpTimeCount--;
                }
                else
                {
                    isJumping = false;
                }
                if (jumpTimeCount < 0)
                {
                    isJumping = false;
                }
            }
        }

        public void GravityJump()
        {
            if (isJumped)
            {
                if (Input.GetKey(Keys.Space))
                {
                    rb.gravityScale = 1;
                }
                else if(rb.velocity.Y <= 0)
                {
                    rb.gravityScale = 3;            
                }
            }
        }

    }
}
