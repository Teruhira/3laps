﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using GameEngine;

namespace _3laps
{
    public class MapChip_FallBlock:GameObject//当たり判定があるMapChip
    {

        Rigidbody rb;
        private float fallTime = 30, fallTimeCount;
        bool fallFlg;
        float fallSpeed;

        public MapChip_FallBlock(Transform transform):base("MapChip_FallBlock", transform)
        {
            tag = "Map_Ground";
            AddComponent(new SpriteRenderer("FallGround", transform, 0, Color.White, 1, 64));
            AddComponent(new RectangleCollider(this, new Square(transform, 64)));
            fallTimeCount = fallTime;
        }

        public override void Start()
        {
        }

        public override void Update()
        {
            if (fallFlg)
            {
                
                if(fallTimeCount < 0 && transform.position.Y < 64 * 20)
                {
                    fallSpeed += Rigidbody.gravity;
                    transform.position.Y += fallSpeed;
                }
                else
                {
                    fallTimeCount--;
                }
            }
        }

        public override void Hit(RectangleCollider rectangleCollider)
        {
            if(rectangleCollider.gameObject.tag == "PlayerFoot")
            {
                fallFlg = true;
            }
        }
    }
}
