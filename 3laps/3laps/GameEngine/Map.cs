﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using _3laps;

namespace GameEngine
{
    public class Map//マップ
    {

        public int mapWidth, mapHeight;//幅、高さ
        public int glidSize;//一マスの大きさ
        public GameObject[,] mapData;//Mapデータ
        public Dictionary<string, GameObject> mapChips;//MapChipを登録する

        public struct Int2
        {
            public int x, y;
            public Int2(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

        }

        
        public Map(string fileName, int glidSize)
        {
            var data = CSVReader.ReadFile(fileName, out mapWidth, out mapHeight);
            this.glidSize = glidSize;
#if DEBUG
            Console.WriteLine(mapWidth + ", " + mapHeight);
#endif 
            mapData = new GameObject[mapWidth, mapHeight];
            for (int i = 0; i < mapHeight; i++)
            {
                for (int k = 0; k < mapWidth; k++)
                {
                    if (data[k, i] == "0")
                    {
                        mapData[k, i] = GameManager.instance.nowScene.gameObjectManager.Instantiate(new MapChip_Air(new Transform(new Vector2(k * glidSize, i * glidSize))));//ここに数値入れて補正
                    }
                    else if (data[k, i] == "1")
                    {
                        mapData[k, i] = GameManager.instance.nowScene.gameObjectManager.Instantiate(new MapChip_Block(new Transform(new Vector2(k * glidSize, i * glidSize))));//ここに数値入れて補正

                    }
                    else if(data[k, i] == "2")
                    {
                        mapData[k, i] = GameManager.instance.nowScene.gameObjectManager.Instantiate(new MapChip_Wall(new Transform(new Vector2(k * glidSize, i * glidSize))));
                    }
                    else if(data[k, i] == "3")
                    {
                        mapData[k, i] = GameManager.instance.nowScene.gameObjectManager.Instantiate(new MapChip_FallBlock(new Transform(new Vector2(k * glidSize, i * glidSize))));
                    }
                }
            }
        }

        public Int2 GetMapPositon(Vector2 positon)
        {
            int x = (int)((positon.X + 32) / glidSize);
            int y = (int)((positon.Y + 32) / glidSize);
            return new Int2(x, y);
        }

        public GameObject GetMapChip(Int2 mapPosition)
        {
            if (mapPosition.x > mapWidth || mapPosition.x < 0 || mapPosition.y > mapHeight || mapPosition.y < 0) return null;
            return mapData[mapPosition.x, mapPosition.y];
        }

        public GameObject GetMapChip(int x, int y)
        {
            if (x >= mapWidth || x < 0 || y >= mapHeight || y < 0) return null;
            return mapData[x, y];
        }

        public bool MapCollision(Rigidbody rigidbody, bool horizontal)
        {
            //グローバル座標をマップ座標に変換
            Int2 position;
            Vector2 tempPosition;
            tempPosition.X = rigidbody.rectangleCollider.square.transform.position.X;
            tempPosition.Y = rigidbody.rectangleCollider.square.transform.position.Y;
            position = GetMapPositon(tempPosition);

            bool result = false;
            //右側にある上中下3枚のマップチップと衝突判定
            for (int i = -3; i < 4; i++)
            {


                for (int k = -3; k < 4; k++)
                {
                    GameObject mapChip = GetMapChip(position.x + k, position.y + i);
                    if (mapChip == null)
                    {

                        continue;
                    }
                    RectangleCollider mapCollider = (RectangleCollider)mapChip.GetComponent("RectangleCollider");

                    if (mapCollider.square.Intersects(rigidbody.rectangleCollider.square) && mapCollider != null)
                    {
                        if (horizontal) {
                            rigidbody.HorizontalHitBack(mapCollider);
                        }
                        else
                        {
                            rigidbody.VerticalHitBack(mapCollider);
                        }
                        result = true;
                    }
                }
            }
            return result; ;
        }

        public bool MapCollision(Rigidbody rigidbody, bool horizontal, List<string> filterTags)
        {
            //グローバル座標をマップ座標に変換
            Int2 position;
            Vector2 tempPosition;
            tempPosition.X = rigidbody.rectangleCollider.square.transform.position.X;
            tempPosition.Y = rigidbody.rectangleCollider.square.transform.position.Y;
            position = GetMapPositon(tempPosition);

            bool result = false;
            //右側にある上中下3枚のマップチップと衝突判定
            for (int i = -3; i < 4; i++)
            {


                for (int k = -3; k < 4; k++)
                {
                    GameObject mapChip = GetMapChip(position.x + k, position.y + i);
                    if (mapChip == null)
                    {

                        continue;
                    }
                    RectangleCollider mapCollider = (RectangleCollider)mapChip.GetComponent("RectangleCollider");

                    if (mapCollider.square.Intersects(rigidbody.rectangleCollider.square) && mapCollider != null)
                    {
                        mapCollider.gameObject.Hit(rigidbody.rectangleCollider);
                        rigidbody.rectangleCollider.gameObject.Hit(mapCollider);
                        if (filterTags.Contains(mapCollider.gameObject.tag))
                        {
                            if (horizontal)
                            {
                                rigidbody.HorizontalHitBack(mapCollider);
                            }
                            else
                            {
                                rigidbody.VerticalHitBack(mapCollider);
                            }
                            result = true;
                        }
                    }
                }
            }
            return result; ;
        }
    }


}
