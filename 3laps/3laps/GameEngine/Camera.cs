﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine;
using OssanAndUmbrella;

namespace GameEngine
{
    public class Camera : GameObject//カメラ　描画範囲の変更
    {
        private float zoomSize;
        public float ZoomSize { get => zoomSize; set => zoomSize = value; }

        public Camera(Transform transform, float zoomSize):base("Camera", transform)
        {
            this.ZoomSize = zoomSize;
            Renderer.instance.camera = this;
        }

        public override void Start()
        {
            
        }

        public override void Update()
        {
        }

        public override void Hit(RectangleCollider rectangleCollider)
        {
            
        }
    }
}
