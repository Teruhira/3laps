﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace _3laps
{
    class Title : Scene//タイトルシーン
    {
        public override void Load()
        {
            gameObjectManager = new GameObjectManager();
            Transform a = new Transform(new Vector2(640, 360));
            gameObjectManager.Instantiate(new Camera(a, 1));
        }

        public override void UnLoad()
        {
        }

        public override void Update()
        {
            if (Input.GetKeyDown(Keys.Space))
            {
                GameManager.instance.ChangeScene(new TestScene());
            }
        }
    }
}
