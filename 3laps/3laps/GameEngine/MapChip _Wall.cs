﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using GameEngine;

namespace _3laps
{
    public class MapChip_Wall:GameObject//当たり判定があるMapChip
    {

        public MapChip_Wall(Transform transform):base("MapChip_Wall", transform)
        {
            tag = "Map_Wall";
            AddComponent(new SpriteRenderer("Wall", transform, 0, Color.White, 1, 64));
            AddComponent(new RectangleCollider(this, new Square(transform, 64)));
        }

        public override void Start()
        {
        }

        public override void Update()
        {

        }

        public override void Hit(RectangleCollider rectangleCollider)
        {
        }
    }
}
