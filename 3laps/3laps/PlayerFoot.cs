﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using GameEngine;

namespace _3laps
{
    class PlayerFoot : GameObject
    {

        private Player player;
        private Rigidbody rb;

        public PlayerFoot(Transform transform, Player player) : base("PlayerFoot", transform)
        {
            this.player = player;
            tag = "PlayerFoot";
            AddComponent(new RectangleCollider(this, new Square(transform, 64, 32)));
            AddComponent(new Rigidbody(transform, (RectangleCollider)GetComponent("RectangleCollider")));
            rb = (Rigidbody)GetComponent("Rigidbody");
            rb.gravityScale = 0;
            rb.simurated = false;
            rb.isTrigger = true;
        }

        public override void Hit(RectangleCollider rectangleCollider)
        {
            player.FootHit(rectangleCollider);
        }

        public override void Start()
        {
            
        }

        public override void Update()
        {
            
        }

        public void SetPosition()
        {
            transform.position = new Vector2(player.transform.position.X, player.transform.position.Y + 32);
        }
    }
}
