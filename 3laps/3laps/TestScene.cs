﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameEngine;

namespace _3laps
{
    class TestScene : Scene
    {
        public override void Load()
        {
            nowMap = new Map("Sample.csv", 64);
            gameObjectManager.Instantiate(new Camera(new Transform(new Vector2(-32, -28)), 1));//カメラ　生成しないとエラー
            gameObjectManager.Instantiate(new Player(new Transform(new Vector2(64, 64 * 15))));//プレイヤーキャラ
            gameObjectManager.Instantiate(new LevelManager());
            
        }

        public override void UnLoad()
        {
            
        }

        public override void Update()
        {
            
        }
    }
}
