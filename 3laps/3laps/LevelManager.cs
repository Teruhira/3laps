﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using GameEngine;

namespace _3laps
{
    class LevelManager : GameObject
    {

        public static LevelManager instance;
        public enum LevelState
        {
            Play,
            Goal,
            GameOver,
            Pause
        }
        public LevelState levelState;

        public int lapCount = 0;
        public const int GOALLAPCOUNT = 3;
        private bool goalFlg;

        public LevelManager() : base("StageManager", new Transform())
        {
            instance = this;
            lapCount = 0;
        }

        public override void Hit(RectangleCollider rectangleCollider)
        {
        }

        public override void Start()
        {
        }

        public override void Update()
        {
            if(lapCount >= GOALLAPCOUNT)
            {
                levelState = LevelState.Goal;
            }
            switch (levelState)
            {
                case LevelState.Goal:
                    if (!goalFlg)
                    {
                        GameManager.instance.nowScene.gameObjectManager.Instantiate(new ClearText(new Transform(new Vector2(64 * 14, 64 * 8))));
                        goalFlg = true;
                    }
                    if (Input.GetKeyDown(Keys.Space))
                    {
                        GameManager.instance.ChangeScene(new Title());
                    }
                    break;
            }
        }
    }
}
