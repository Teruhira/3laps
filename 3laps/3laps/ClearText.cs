﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine;

namespace _3laps
{
    class ClearText : GameObject
    {
        public ClearText(Transform transform) : base("Text", transform)
        {
            AddComponent(new SpriteRenderer("ClearText", 1920, 1080, transform, 1));
            
        }

        public override void Hit(RectangleCollider rectangleCollider)
        {
        }

        public override void Start()
        {
        }

        public override void Update()
        {
        }
    }
}
